import sys
sys.path.append("/usr/src/volatility")
# read name of monitored VM and pid of process that should be altered
name = sys.argv[1]
if name.find("one-") == -1:
    print "The first parameter is the VMs name. It must start with 'one-' followed by the ID."
    sys.exit()
try:
    searchedPid = int(sys.argv[2])
except:
    print "Second parameter is the searched pid. It must be a natural number."
    sys.exit()

origargv = sys.argv
sys.argv = [sys.argv[0], "-f", "/mnt/mem", "--profile", "LinuxDebian8x64"]
## Define vmifs image and profile
import os
import socket
import time
import pyvmi
import volatility.conf as conf
import volatility.utils as utils
config = conf.ConfObject()
import volatility.obj as obj
import volatility.addrspace as addrspace
import volatility.registry as registry

registry.PluginImporter()
registry.register_global_options(config, addrspace.BaseAddressSpace)
## Main program starts here:
# Initialize address space (same as a=addrspace() in linux_volshell)

a=utils.load_as(config)
p=a.profile
# Lookup kernel symbol pointing to first task
init_addr = p.get_symbol("init_task")
# Create python object for this task
tasks1 = obj.Object("task_struct", vm=a, offset=init_addr)
# Magic module iterator via linked list at element list in init_task
g=tasks1.tasks.list_of_type("task_struct", "tasks")
tasklist=list(g)
# find the position of the process that should be edited in the list of tasks
found = False
counter = 0
while counter < len(tasklist) and found == False:
    if tasklist.__getitem__(counter).pid == searchedPid:
            found =True
    else:
        counter = counter+1
if found == False:
    print "No process with this pid found"
    sys.exit()

# find the process that should be edited in the address space
if counter != 0:
    searchedTaskOffset = tasklist.__getitem__(counter-1).tasks.next - 0x280
    searchedTask = obj.Object("task_struct", vm=a, offset=searchedTaskOffset)
else:
    searchedTaskOffset = tasks1.tasks.next - 0x280
    searchedTask = obj.Object("task_struct", vm=a, offset=searchedTaskOffset)


# initialize the pyvmi connection to the monitored VM
try:
    vmi=pyvmi.init(name, "complete")
except:
    print "Can't connect to VM with id " + name
    sys.exit()

#get the credentials of the init_task
init_cred = tasks1.cred.v()
init_realcred = tasks1.real_cred.v()

#get the credentials offset of the searched task
searched_cred_va = searchedTask.cred.obj_offset
searched_cred_pa = a.vtop(searched_cred_va)
searched_realcred_va = searchedTask.real_cred.obj_offset
searched_realcred_pa = a.vtop(searched_realcred_va)

#set the credentials of the searched task to the ones of the init_task
vmi.write_64_pa(searched_cred_pa, init_cred)
vmi.write_64_pa(searched_realcred_pa, init_realcred)