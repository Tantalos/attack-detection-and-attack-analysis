package dhl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SysCall {

	private static final Thread.UncaughtExceptionHandler libvmtraceErrorHandler = new Thread.UncaughtExceptionHandler() {
		public void uncaughtException(Thread t, Throwable e) {
			e.printStackTrace();
			System.err.println("Error whilest connecting to system call stream");
			System.exit(1);
		}
	};
	
	private final static MonitorSessionManager MONITOR_SESSION_MANAGER = new MonitorSessionManager();

	
	public static void main(String[] args) throws Exception {
		if (args == null || args.length < 1 || "".equals(args[0])) {
			System.out.println("libvmtrace <vmname>. Add target machine");
			return;
		}

		

		final SystemCallCrawler systemCallCrawler = new SystemCallCrawler(args[0]);
		systemCallCrawler.connectSystemCallSync(MONITOR_SESSION_MANAGER);
		MONITOR_SESSION_MANAGER.close();
	}

}