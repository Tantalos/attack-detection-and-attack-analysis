package dhl;

public interface SystemCallListener {
	
	public void systemCallback(SystemCall systemCall, Throwable errorHandle);
}
