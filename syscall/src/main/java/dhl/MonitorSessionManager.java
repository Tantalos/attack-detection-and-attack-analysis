package dhl;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MonitorSessionManager implements SystemCallListener {
	private static enum FdType {
		WRITE_FROM_BASH(1), WRITE_FROM_BASH2(2), INPUT_ENCRYPT(3), WRITE_TO_BASH(9), READ_TO_BASH(11);
		private int code;

		private FdType(int code) {
			this.code = code;
		}

		public boolean equalsCode(int code) {
			return this.code == code;
		}
	};
	
	private static List<FdType> track = Arrays.asList(FdType.READ_TO_BASH, FdType.WRITE_FROM_BASH,
			FdType.WRITE_FROM_BASH2);
	
	private final Set<Integer> monitoredProcesses = new HashSet<Integer>();
	private final Map<Integer, PrintWriter> printWriter = new HashMap<>();


	public MonitorSessionManager() {
		// cleanup resources if program wasn't terminated appropriate
		final Thread shutdownHook = new Thread() {
			public void run() {
				MonitorSessionManager.this.close();
			}
		};
		Runtime.getRuntime().addShutdownHook(shutdownHook);
	}
	
	private void append(SystemCall sys) {
		Integer processID = sys.getPid();
		
		if(printWriter.containsKey(processID)) {
			printWriter.get(processID).print(sys.getBuf());;
		}
	}
	
	private void trackSession(Integer processID) {
		if(monitoredProcesses.contains(processID)) {
			return;
		}
		
		monitoredProcesses.add(processID);
		try {
			PrintWriter writer = new PrintWriter("ssh_session" + processID + ".txt", "UTF-8");
			printWriter.put(processID, writer);
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void systemCallback(SystemCall systemCall, Throwable errorHandle) {
		if (systemCall == null) {
			return;
		}

		// do your analysis here

		String path = systemCall.getPath();
		// regex. matches every path which ends with sshd
		if (path != null && path.matches(".*sshd$")) {
			System.out.println("SSHD session found wit pid: " + systemCall.getPid());
			trackSession(systemCall.getPid());
		}

		// writer.println(systemCall.toString());

		if (monitoredProcesses.contains(systemCall.getPid())) {

			if (track.stream().anyMatch(x -> x.equalsCode(systemCall.getFd()))) {
				System.out.print(systemCall.getBuf());
				
				append(systemCall);
				
			}
		}

	}

	public void close() {
		printWriter.forEach((k, v) -> v.close());
	}
}
