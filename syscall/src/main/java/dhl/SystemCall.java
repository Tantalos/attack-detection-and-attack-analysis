package dhl;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(includeFieldNames = true)
public class SystemCall {
	//
	// {
	// "time":1484395482,
	// "id":1,
	// "logtype":"sys:syscall",
	// "buf":"\u0001\u0000\u0000\u0000\u0000\u0000\u0000\u0000",
	// "syscall_name":"SyS_write",
	// "dtb":442769408,
	// "fd":6,
	// "param0":6,
	// "param1":-237163776,
	// "param2":8,
	// "param3":-335528788,
	// "param4":5,
	// "param5":0,
	// "pid":4035,
	// "return_value":8,
	// "rip":-210257283,
	// "rsp":443776896,
	// "size":8,
	// "syscall_nr":1
	// }
	
//	"args": ["/usr/sbin/sshd","-D","-R"],
//	"env": ["LANG=en_US.UTF-8","PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin","SSHD_OPTS="],
//	"path": "/usr/sbin/sshd",
	
//	{
//		"time": 1484503093,
//		 "id": 1, "logtype": "sys:syscall",
//		 "args": ["/usr/sbin/sshd","-D","-R"],
//		 "env": ["LANG=en_US.UTF-8","PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin","SSHD_OPTS="],
//		 "path": "/usr/sbin/sshd",
//		 "syscall_name": "stub_execve",
//		 "dtb": 464736256,
//		 "param0": 2140758096,
//		 "param1": 2140788976,
//		 "param2": 2140758160,
//		 "param3": -174544383,
//		 "param4": 7,
//		 "param5": 8,
//		 "pid": 1748,
//		 "rip": 2097773287,
//		 "rsp": 452198272,
//		 "syscall_nr": 59
//		}
//	
	

	private int time;
	private int id;
	private String logtype;
	private String buf;
	private String syscall_name;
	private int dtb;
	private int fd;
	private int param0;
	private int param1;
	private int param2;
	private int param3;
	private int param4;
	private int param5;
	private int pid;
	private int return_value;
	private int rip;
	private int rsp;
	private int size;
	private int syscall_nr;
	private List<String> args;
	private List<String> env;
	private String path;


}
