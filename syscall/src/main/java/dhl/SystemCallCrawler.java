package dhl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SystemCallCrawler {

	private final String vmTargetName;
	private Process systemCallCrawler;
	
	private static final List<String> ignoreStaticOutput = Arrays.asList(
			".*Sending kill signal, please wait a few seonds.*",
			".*Preparing to stop: handler will not re-inject BP any more.*",
			".*Removing BP from VM memory (without de-registering).*",
			".*Finally, deregistering everything.*",
			".*start listening on eth0 with filter.*",
			".*initialized net outprocesser.*"
			);

	public SystemCallCrawler(String vmTargetName) {
		if (vmTargetName == null || "".equals(vmTargetName)) {
			throw new IllegalArgumentException("Illegal argument. Vm target name musn't be empty");
		}
		this.vmTargetName = vmTargetName;
		
		
		final Thread shutdownHook = new Thread() {
			public void run() {
				System.out.println("Program received termination signal");
				SystemCallCrawler.this.kill();
			}
		};
		Runtime.getRuntime().addShutdownHook(shutdownHook);
	
	}

	public void connectSystemCallSync(SystemCallListener systemCallListener)
			throws IOException, IllegalStateException {
		if (systemCallCrawler != null) {
			throw new IllegalStateException(
					"An instance of libvmtrace is already running. This version can only handle one "
							+ "instance of libvmtrace. Inappropriate terminaction of libvmtrace will cause fatal damage");
		}

		ProcessBuilder processBuilder = new ProcessBuilder("/root/libvmtrace2/bin/csec", vmTargetName);
		processBuilder.redirectErrorStream(true);
		systemCallCrawler = processBuilder.start();

		String line;

		Reader inStreamReader = new InputStreamReader(systemCallCrawler.getInputStream());
		BufferedReader in = new BufferedReader(inStreamReader);

		System.out.println("Stream started");
		while ((line = in.readLine()) != null) {
			final String finalLine = line;
			if(ignoreStaticOutput.stream().anyMatch(x -> finalLine.matches(x))) {
				continue;
			}
			
			try {
				SystemCall systemCall = parseSystemCall(line);
				systemCallListener.systemCallback(systemCall, null);
			} catch (Throwable e) {
				// if error occurs during parsing, pass an error handle
				systemCallListener.systemCallback(null, e);
			}
		}

		in.close();
		System.out.println("Stream Closed");
	}

	public SystemCall parseSystemCall(String jsonString) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		SystemCall sc = mapper.readValue(jsonString, SystemCall.class);

		return sc;
	}

	public void connectSystemCallAsync(final SystemCallListener systemCallListener, Thread.UncaughtExceptionHandler h) {
		Thread systemCallCrawler = new Thread(new Runnable() {
			public void run() {
				try {
					connectSystemCallSync(systemCallListener);
				} catch (IOException e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		});

		// daemon thread gets terminated if main thread is terminated. Watch dog will handle remaining running processes
		systemCallCrawler.setDaemon(true);
		systemCallCrawler.setUncaughtExceptionHandler(h);
		systemCallCrawler.start();
		
		// destroys process if thread is terminated unexpectedly
		new WatchDog(systemCallCrawler);
	}

	public void kill() throws IllegalStateException {
		if (systemCallCrawler == null) {
			System.out.println("Warning: No process is running. Cannot killprocess!");
			return; 
		}
		
		synchronized (systemCallCrawler) {
			systemCallCrawler.destroy();
			
			if(systemCallCrawler.isAlive()) {
				System.out.println("libvmtrace termination signal was sent but process is still running");
			} else {
				System.out.println("libvmtrace was terminated and stopped with return value " + systemCallCrawler.exitValue());
				systemCallCrawler = null;
			}
		}
	}

	public boolean isRunning() {
		return systemCallCrawler != null;
	}

	/**
	 * WatchDog which checks if the process-managing thread was
	 * terminated. If so a termination signal is sent to the process. This
	 * avoids a running dead process when the thread was terminated
	 * unexpectedly.
	 *
	 */
	private class WatchDog extends Thread {
		private Thread thread;

		public WatchDog(Thread thread) {
			this.thread = thread;
		}

		@Override
		public void run() {
			super.run();

			while (thread.isAlive()) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			// thread is already terminated. Send termination signal to libvmtrace
			SystemCallCrawler.this.kill();
		}

	}

}
