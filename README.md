# For more information see the [Wiki](https://bitbucket.org/Tantalos/attack-detection-and-attack-analysis/wiki/Home)!

This project are prototypes of monitoring methods. It uses the approach of virtual machine introspection by observation of memory to monitore and manipulate virtual machines.

The project is based on instructions which can be found [here](https://gandra.fim.uni-passau.de/git/csec_ws2016/group_2_project_2/src/master/Project2.pdf). It consists of 3 parts:

1. [Malware detection using VMI with volatility](https://bitbucket.org/Tantalos/attack-detection-and-attack-analysis/wiki/Malware%20detection%20using%20VMI%20with%20volatility)
2. [Virtual machine manipulation with libvmi](https://bitbucket.org/Tantalos/attack-detection-and-attack-analysis/wiki/Virtual%20machine%20manipulation%20with%20libvmi)
3. [System observation with system call tracing](https://bitbucket.org/Tantalos/attack-detection-and-attack-analysis/wiki/System%20observation%20with%20system%20call%20tracing) 
