package proj2;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RunInformation {

	List<ProcessInformation> processInfos;

	public List<ProcessInformation> getProcessInfos() {
		return processInfos;
	}

	public void setProcessInfos(List<ProcessInformation> processInfos) {
		this.processInfos = processInfos;
	}

	private List<KernelInformation> kernelInfos;

	public List<KernelInformation> getKernelInfos() {
		return kernelInfos;
	}

	public void setKernelInfos(List<KernelInformation> kernelInfos) {
		this.kernelInfos = kernelInfos;
	}

	private List<KernelInformation> hiddenKernelInfos;

	public List<KernelInformation> getHiddenKernelInfos() {
		return hiddenKernelInfos;
	}

	public void setHiddenKernelInfos(List<KernelInformation> hiddenKernelInfos) {
		this.hiddenKernelInfos = hiddenKernelInfos;
	}

	private List<NetworkInformation> networkInfos;

	public List<NetworkInformation> getNetworkInfos() {
		return networkInfos;
	}

	public void setNetworkInfos(List<NetworkInformation> networkInfos) {
		this.networkInfos = networkInfos;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (KernelInformation kernelInformation : kernelInfos) {
			sb.append(kernelInformation.toString());
			sb.append("\n");

		}

		sb.append("\n");

		for (KernelInformation kernelInformation : hiddenKernelInfos) {
			sb.append(kernelInformation.toString());
			sb.append("\n");
		}

		sb.append("\n");

		for (ProcessInformation processInformation : processInfos) {
			sb.append(processInformation.toString());
			sb.append("\n");
		}

		sb.append("\n");

		for (NetworkInformation netInfos : networkInfos) {
			sb.append(netInfos.toString());
			sb.append("\n");
		}
		return sb.toString();
	}

}
