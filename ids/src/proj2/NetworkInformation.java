package proj2;

import java.io.Serializable;
import java.util.Objects;

import javax.xml.bind.annotation.XmlElement;

public class NetworkInformation implements Serializable {

	private static final long serialVersionUID = 1L;

	private String line;

	public NetworkInformation() {
	}

	public NetworkInformation(String line) {
		this.line = line;
	}

	@Override
	public String toString() {
		return line;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!Objects.equals(this.getClass(), obj.getClass())) {
			return false;
		}

		NetworkInformation other = (NetworkInformation) obj;
		return Objects.equals(this.getLine(), other.getLine());
	}

	public String getLine() {
		return line;
	}

	@XmlElement
	public void setLine(String line) {
		this.line = line;
	}

}
