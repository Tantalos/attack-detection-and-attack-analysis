package proj2;

import java.io.Serializable;
import java.util.Objects;

import javax.xml.bind.annotation.XmlElement;

public class ProcessInformation implements Serializable {

	private static final long serialVersionUID = 1L;

	private int userId;
	private int pid;
	private String cmd;

	public ProcessInformation() {
	}

	public ProcessInformation(int userId, int pid, String cmd) {
		super();
		this.userId = userId;
		this.pid = pid;
		this.cmd = cmd;
	}

	@XmlElement
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@XmlElement
	public void setPid(int pid) {
		this.pid = pid;
	}

	@XmlElement
	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public int getUserId() {
		return userId;
	}

	public int getPid() {
		return pid;
	}

	public String getCmd() {
		return cmd;
	}

	@Override
	public String toString() {
		return "proc info[ pid: " + pid + ",\tuserId: " + userId + ",\tcmd: " + cmd + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!Objects.equals(this.getClass(), obj.getClass())) {
			return false;
		}

		ProcessInformation other = (ProcessInformation) obj;

		boolean sameUserId = Objects.equals(userId, other.getUserId());
		boolean samePid = Objects.equals(pid, other.getPid());
		boolean sameCmd = Objects.equals(cmd, other.getCmd());

		return sameUserId && samePid && sameCmd;
	}
}