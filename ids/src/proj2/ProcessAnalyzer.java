package proj2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class ProcessAnalyzer {

	private static List<String> baseCmd = Arrays.asList("python", "/usr/src/volatility/vol.py", "volatility", "-f",
			"/mnt/mem", "--profile", "LinuxDebian8x64");

	public static void main(String[] args) throws IOException {
		System.out.println("==============================");
		System.out.println("===> PROGRAM START");
		System.out.println("==============================");

		if (args == null) {
			terminateWithErrorMessage("no parameters given");
		}

		if (args.length > 0) {
			System.out.println(
					"hint: machine " + args[0] + " has to be mounted to /mnt/mem and volatility has to be present");
			System.out.println();
		}

		switch (args.length) {
		case 3: // make one run and save it to file
			if (!Objects.equals("-store", args[1])) {
				terminateWithErrorMessage("unknown parameters");
			}

			writeRunToDisk(getRunInfos(), args[2]);
			break;

		case 5: // make one run save to file and compare with another one
			if (!Objects.equals("-store", args[1]) || !Objects.equals("-diff", args[3])) {
				terminateWithErrorMessage("unknown parameters");
			}

			RunInformation run = getRunInfos();
			compareRuns(run, readFromDisk(args[4]));
			break;

		default:
			terminateWithErrorMessage("unknown parameters");
			break;
		}

	}

	private static void terminateWithErrorMessage(String string) {
		System.err.println(string);
		System.exit(0);
	}

	private static void compareRuns(RunInformation one, RunInformation two) {

		// compare processes
		System.out.println("==> comparing processes:");
		// make shallow copy to delete links
		List<ProcessInformation> oneProc = new ArrayList<>(one.getProcessInfos());
		List<ProcessInformation> twoProc = new ArrayList<>(two.getProcessInfos());
		// remove whats the same
		oneProc.removeAll(two.getProcessInfos());
		twoProc.removeAll(one.getProcessInfos());

		if (!oneProc.isEmpty()) {
			System.out.println("==> unique to first run");
			oneProc.forEach(x -> System.out.println(x));
		}
		if (!twoProc.isEmpty()) {
			System.out.println("==> unique to second run");
			twoProc.forEach(x -> System.out.println(x));
		}

		System.out.println("==> comparing processess finished!");
		System.out.println();

		// compare kernel modules
		System.out.println("==> comparing kernel modules:");
		List<KernelInformation> oneKernel = new ArrayList<>(one.getKernelInfos());
		List<KernelInformation> twoKernel = new ArrayList<>(two.getKernelInfos());
		oneKernel.removeAll(two.getKernelInfos());
		twoKernel.removeAll(one.getKernelInfos());

		if (!oneKernel.isEmpty()) {
			System.out.println("==> unique to first run");
			oneKernel.forEach(x -> System.out.println(x));
		}
		if (!twoKernel.isEmpty()) {
			System.out.println("==> unique to second run");
			twoKernel.forEach(x -> System.out.println(x));
		}

		System.out.println("==> comparing kernel modules finished!");
		System.out.println();

		// compare hidden kernel modules
		System.out.println("==> comparing hidden kernel modules:");
		oneKernel = new ArrayList<>(one.getHiddenKernelInfos());
		twoKernel = new ArrayList<>(two.getHiddenKernelInfos());
		oneKernel.removeAll(two.getHiddenKernelInfos());
		twoKernel.removeAll(one.getHiddenKernelInfos());

		if (!oneKernel.isEmpty()) {
			System.out.println("==> unique to first run");
			oneKernel.forEach(x -> System.out.println(x));
		}

		if (!twoKernel.isEmpty()) {
			System.out.println("==> unique to second run");
			twoKernel.forEach(x -> System.out.println(x));
		}
		System.out.println("==> comparing hidden kernel modules finished!");
		System.out.println();

		// compare network information
		System.out.println("==> comparing network information:");

		List<NetworkInformation> oneNet = new ArrayList<>(one.getNetworkInfos());
		List<NetworkInformation> twoNet = new ArrayList<>(two.getNetworkInfos());
		// remove whats the same
		oneNet.removeAll(two.getNetworkInfos());
		twoNet.removeAll(one.getNetworkInfos());

		if (!oneNet.isEmpty()) {
			System.out.println("==> unique to first run");
			oneNet.forEach(x -> System.out.println(x));
		}
		if (!twoNet.isEmpty()) {
			System.out.println("==> unique to second run");
			twoNet.forEach(x -> System.out.println(x));
		}

		System.out.println("==> comparing network information finished!");
		System.out.println();
	}

	private static RunInformation readFromDisk(String filename) {
		try {
			File file = new File(filename);
			JAXBContext jaxbContext = JAXBContext.newInstance(RunInformation.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			RunInformation run = (RunInformation) jaxbUnmarshaller.unmarshal(file);
			return run;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static boolean writeRunToDisk(RunInformation run, String filename) {
		try {
			File file = new File(filename);
			JAXBContext jaxbContext = JAXBContext.newInstance(RunInformation.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(run, file);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static RunInformation getRunInfos() throws IOException {
		System.out.println("getting process informations...");
		List<ProcessInformation> processInfos = getProcessInformations();

		System.out.println("getting kernel module informations...");
		List<KernelInformation> kernelInfos = getKernelModules();

		System.out.println("getting hidden kernel module informations...");
		List<KernelInformation> hiddenKernelInfos = getHiddenKernelModules();

		System.out.println("getting network informations...");
		List<NetworkInformation> netInfos = getNetworkInformation();

		RunInformation run = new RunInformation();
		run.setProcessInfos(processInfos);
		run.setKernelInfos(kernelInfos);
		run.setHiddenKernelInfos(hiddenKernelInfos);
		run.setNetworkInfos(netInfos);
		return run;
	}

	private static List<NetworkInformation> getNetworkInformation() throws IOException {
		List<String> process_args = new ArrayList<String>(baseCmd);
		process_args.add("linux_netstat");
		Runtime r = Runtime.getRuntime();
		Process p = r.exec(process_args.toArray(new String[] {}));

		String s;
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

		List<NetworkInformation> networkInfos = new ArrayList<NetworkInformation>();

		stdInput.readLine(); // drop header

		while ((s = stdInput.readLine()) != null) {
			NetworkInformation newNetInfo = new NetworkInformation(s);
			networkInfos.add(newNetInfo);
		}

		return networkInfos;
	}

	private static List<KernelInformation> getHiddenKernelModules() throws IOException {
		List<String> process_args = new ArrayList<String>(baseCmd);
		process_args.add("linux_hidden_modules");
		Runtime r = Runtime.getRuntime();
		Process p = r.exec(process_args.toArray(new String[] {}));

		String s;
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

		List<KernelInformation> kernelInfos = new ArrayList<KernelInformation>();

		for (int i = 0; i < 2; i++) {
			stdInput.readLine(); // drop header etc until data table starts
		}

		while ((s = stdInput.readLine()) != null) {
			String[] cleanedBlocks = Arrays.stream(s.split("\\s")).filter(x -> x != null && x.length() > 0)
					.toArray(String[]::new);

			String module = cleanedBlocks[1];
			String offset = cleanedBlocks[0];

			KernelInformation newModule = new KernelInformation(module, offset, -1);
			kernelInfos.add(newModule);
		}

		return kernelInfos;
	}

	private static List<KernelInformation> getKernelModules() throws IOException {
		List<String> process_args = new ArrayList<String>(baseCmd);
		process_args.add("linux_lsmod");
		Runtime r = Runtime.getRuntime();
		Process p = r.exec(process_args.toArray(new String[] {}));

		String s;
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

		List<KernelInformation> kernelInfos = new ArrayList<KernelInformation>();

		stdInput.readLine(); // drop header

		while ((s = stdInput.readLine()) != null) {
			String[] cleanedBlocks = Arrays.stream(s.split("\\s")).filter(x -> x != null && x.length() > 0)
					.toArray(String[]::new);

			String module = cleanedBlocks[1];
			int size = Integer.valueOf(cleanedBlocks[2]);
			String offset = cleanedBlocks[0];

			KernelInformation newModule = new KernelInformation(module, offset, size);
			kernelInfos.add(newModule);
		}

		return kernelInfos;
	}

	private static List<ProcessInformation> getProcessInformations() throws IOException {
		List<String> process_args = new ArrayList<String>(baseCmd);
		process_args.add("linux_pslist");
		Runtime r = Runtime.getRuntime();
		Process p = r.exec(process_args.toArray(new String[] {}));

		String s;
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

		for (int i = 0; i < 3; i++) {
			stdInput.readLine(); // drop header etc until data table starts
		}

		List<ProcessInformation> procs = new ArrayList<ProcessInformation>();

		while ((s = stdInput.readLine()) != null) {
			String[] cleanedBlocks = Arrays.stream(s.split("\\s")).filter(x -> x != null && x.length() > 0)
					.toArray(String[]::new);

			int user = Integer.parseInt(cleanedBlocks[3]);
			int pid = Integer.parseInt(cleanedBlocks[2]);
			String cmd = cleanedBlocks[1];

			ProcessInformation newProc = new ProcessInformation(user, pid, cmd);

			procs.add(newProc);

		}

		return procs;
	}
}
