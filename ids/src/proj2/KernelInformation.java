package proj2;

import java.io.Serializable;
import java.util.Objects;

import javax.xml.bind.annotation.XmlElement;

public class KernelInformation implements Serializable {

	private static final long serialVersionUID = 1L;

	private String module;
	private String offset;
	private int size;

	public KernelInformation() {
	}

	public KernelInformation(String module, String offset, int size) {
		super();
		this.module = module;
		this.offset = offset;
		this.size = size;
	}

	@XmlElement
	public void setModule(String module) {
		this.module = module;
	}

	@XmlElement
	public void setOffset(String offset) {
		this.offset = offset;
	}

	@XmlElement
	public void setSize(int size) {
		this.size = size;
	}

	public String getModule() {
		return module;
	}

	public String getOffset() {
		return offset;
	}

	public int getSize() {
		return size;
	}

	@Override
	public String toString() {
		return "kernel module: [module: " + module + ",\tsize: " + size + ",\toffset: " + offset + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!Objects.equals(this.getClass(), obj.getClass())) {
			return false;
		}

		KernelInformation other = (KernelInformation) obj;

		boolean sameModuleName = Objects.equals(module, other.getModule());
		boolean sameOffset = Objects.equals(offset, other.getOffset());
		boolean sameSize = Objects.equals(size, other.getSize());

		return sameModuleName && sameOffset && sameSize;
	}

}
